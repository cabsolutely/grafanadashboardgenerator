var dashboardGen = new (require('./grafana-dash/gen-dashboard.js'))();
var http = require('http');
var fs = require('fs');

var requiredServiceOpts = [
    'serviceName',
    'projectName'
];

function GrafanaDashboardGenerator() {}

GrafanaDashboardGenerator.prototype.generateDashboard = function(serviceOpts, forced) {

    if (!serviceOpts) {
        console.log('ERROR: Missing serviceOpts parameter. Usage: grafanaDashboardGenerator.generateDashboard(serviceOpts, forced);');
        return;
    }
    if (!process.env.GRAFANA_URL) {
        console.log('ERROR: GRAFANA_URL enviroment variable is null');
        return;
    }
    if (!process.env.GRAFANA_USER) {
        console.log('ERROR: GRAFANA_USER enviroment variable is null');
        return;
    }
    if (!process.env.GRAFANA_PASSWORD) {
        console.log('ERROR: GRAFANA_PASSWORD enviroment variable is null');
        return;
    }

    if (process.env.HOSTNAME !== process.env.DOCKERCLOUD_SERVICE_HOSTNAME + '-1') {
        return;
    }

    var serviceName = serviceOpts.serviceName;
    var grafanaUrl = process.env.GRAFANA_URL;
    var grafanaUser = process.env.GRAFANA_USER;
    var grafanaPassword = process.env.GRAFANA_PASSWORD;
    var dashboardTitle = serviceOpts.dashboardTitle ? serviceOpts.dashboardTitle : serviceName;
    var temp = grafanaUrl.split('://')[1].split(':');
    var grafanaHostname = temp[0];
    var grafanaPort = temp[1].split('/')[0];
    var grafanaPath = temp[1].split(grafanaPort)[1] + dashboardTitle;
    var grafanaCookie = '';

    var body = JSON.stringify({
        email: '',
        password: grafanaPassword,
        user: grafanaUser
    });

    var request = new http.ClientRequest({
        hostname: grafanaHostname,
        port: grafanaPort,
        path: "/login",
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Content-Length": Buffer.byteLength(body)
        }
    });

    request.end(body);

    request.on('response', function (response) {
        var cookieArray = response.headers['set-cookie'];
        grafanaCookie = cookieArray[0].split(' ')[0];
        for (var i = 1; i < cookieArray.length; i++) {
            var item = cookieArray[i];
            grafanaCookie += ' ' + item.split(' ')[0];
        }
        if (!forced) {
            var headers = {
                'Cookie': grafanaCookie
            };
            var getOpts = {
                host: grafanaHostname,
                port: grafanaPort,
                path: grafanaPath,
                headers: headers
            };
            http.get(getOpts, function onResult(res) {
                if (res.statusCode === 404) {
                    generate();
                }
            });
        }
        else {
            generate();
        }
    });

    request.on('error', function (error) {
        console.log(error);
    });

    function generate() {
        readThriftFunctions(function onRes(functions) {
            var relevantFunctions = [];
            for (var i = 0; i < functions.length; i++) {
                var f = functions[i];
                if (f.slice(0, 4) !== 'test') {
                    relevantFunctions.push(f);
                }
            }
            var containers = [
                {
                    containerName: process.env.HOSTNAME,
                    host: process.env.HOST,
                    port: process.env.PORT
                }
            ];

            var hasMore = true;
            var i = 2;
            while(hasMore && process.env.DOCKERCLOUD_SERVICE_HOSTNAME) {
                var prefix = process.env.DOCKERCLOUD_SERVICE_HOSTNAME.toUpperCase() + '_' + i + '_ENV_';
                if (!process.env[prefix + 'PORT']) {
                    hasMore = false;
                    break;
                }
                var container = {
                    containerName: process.env[prefix + 'DOCKERCLOUD_CONTAINER_HOSTNAME'],
                    host: process.env[prefix + 'DOCKERCLOUD_IP_ADDRESS'].split('/')[0],
                    port: process.env[prefix + 'PORT']
                };
                containers.push(container);
                i++;
            }

            var grafanaOpts = {
                url: grafanaUrl,
                cookie: grafanaCookie
            };

            var sOpts = {
                serviceName: serviceName,
                projectName: serviceOpts.projectName,
                dashboardTitle: dashboardTitle,
                functions: relevantFunctions,
                nodeEnv: process.env.NODE_ENV,
                company: process.env.COMPANY,
                containers: containers
            };

            dashboardGen.generateDashboard(grafanaOpts, sOpts);
        });
    }

}

function missingParameter(data, requiredPoperties) {
    for (var i = 0; i < requiredPoperties.length; i++) {
        var prop = requiredPoperties[i];
        if (!data.hasOwnProperty(prop) ||
            data[prop] === null || data[prop] === undefined) {
            return prop;
        }
    }
    return false;
}

function readThriftFunctions(cb) {
    var thriftPath = 'thrift/service.thrift';

    fs.readFile(thriftPath, function(err, data) {
        if (err) {
            return console.log(err);
        }
        var thriftFile = data.toString();
        var thriftFileSplitted = thriftFile.split('(');
        var funcNames = [];
        for (var i = 0; i < thriftFileSplitted.length - 1; i++) {
            var temp = thriftFileSplitted[i];
            var tempArray = temp.split(' ');
            temp = tempArray[tempArray.length - 1];
            if (temp !== '') {
                funcNames.push(temp);
            }
        }
        cb(funcNames);
    });
}

module.exports = GrafanaDashboardGenerator;
