// Copyright (c) 2016 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

'use strict';

var getConfig = require('zero-config');
var grafana = require('grafana-dash-gen');
var path = require('path');
var strformat = require('strformat');
var configDir = __dirname;

var config = getConfig(configDir, {
    dcValue: "test"
});

var requiredGrafanaOpts = ['cookie', 'url'];
var requiredServiceOpts = [
    'containers',
    'serviceName',
    'nodeEnv',
    'functions',
    'projectName'
];

function DashboardGen() {}

DashboardGen.prototype.generateDashboard = function(grafanaOpts, serviceOpts) {
    if (!grafanaOpts || !serviceOpts) {
        console.log('ERROR: Missing parameter(s). Usage: dashboardGen.generateDashboard(grafanaOpts, serviceOpts);');
        return;
    }
    var missingParam = missingParameter(grafanaOpts, requiredGrafanaOpts);
    if (missingParam) {
        console.log('ERROR: Missing parameter ' + missingParam);
        return;
    }

    grafana.configure({
        cookie: grafanaOpts.cookie,
        url: grafanaOpts.url
    });

    function substituteVariables(vars) {
        // Iteratively subsitutes variables
        var doSubsitutePossible = function(vars) {
            var varsDone = {};
            var keys = Object.keys(vars);
            var allDone = true;
            for(var i = 0,length = keys.length; i < length; i++) {
                if(vars[keys[i]].indexOf('{') >= 0) {
                    allDone = false;
                } else {
                    varsDone[keys[i]] = vars[keys[i]];
                }
            }
            if(allDone) {
                return true;
            }
            for(var i = 0,length = keys.length; i < length; i++) {
                if(vars[keys[i]].indexOf('{') >= 0) {
                    vars[keys[i]] = strformat(vars[keys[i]], varsDone);
                }
            }
            return false;
        }

        // We don't need to go deeper than 100, prevents circular substitution
        var i = 0;
        while(i < 100 && !doSubsitutePossible(vars)) {
            i++;
        }
    }

    var templateVariables = config.get('gen-dashboard.variable');
    substituteVariables(templateVariables);

    var containers = serviceOpts.containers;
    var serviceName = serviceOpts.serviceName;
    var temp = serviceName.split('-');
    var uppercasedServiceName = temp[0].charAt(0).toUpperCase() + temp[0].slice(1);
    for (var i = 1; i < temp.length; i++) {
        uppercasedServiceName += temp[i].charAt(0).toUpperCase() + temp[i].slice(1);
    }
    var nodeEnv = serviceOpts.nodeEnv;
    var company = serviceOpts.company ? serviceOpts.company : '';
    var projectName = serviceOpts.projectName;

    function getTargets(paths, multiple, funcName) {
        if (typeof paths === 'string') {
            paths = [paths];
        }
        var targets = [];
        var n = multiple ? containers.length : 1;
        for (var i = 0; i < paths.length; i++) {
            var path = paths[i];
            var baseTarget = config.get('gen-dashboard.template.' + path);
            var tempTarget = baseTarget;

            for (var j = 0; j < n; j++) {
                var c = containers[j];
                if (tempTarget.indexOf('$RINGPOP_HEADER') !== -1) {
                    tempTarget = getHeader(c, true) + tempTarget.split('$RINGPOP_HEADER')[1];
                } else {
                    tempTarget = getHeader(c, false) + tempTarget;
                }
                if (tempTarget.indexOf('$SERVICENAMELOWER') !== -1) {
                    tempTarget = tempTarget.split('$SERVICENAMELOWER').join(company + serviceName);
                }
                if (tempTarget.indexOf('$SERVICENAME') !== -1) {
                    tempTarget = tempTarget.split('$SERVICENAME').join(uppercasedServiceName);
                }
                if (tempTarget.indexOf('$FUNCNAME') !== -1 && funcName) {
                    tempTarget = tempTarget.split('$FUNCNAME').join(funcName);
                }
                targets.push(new grafana.Target(tempTarget));
                tempTarget = baseTarget;
            }
        }
        return targets;
    }

    function getHeader(container, isRingpopNeeded) {
        var baseHeader = projectName + nodeEnv + container.containerName;
        if (isRingpopNeeded) {
            var ringpopHeader = '.ringpop.' + container.host.split('.').join('_') + '_' + container.port;
            baseHeader += ringpopHeader;
        }
        return baseHeader;
    }

    var dashboard = new grafana.Dashboard({
        title: serviceOpts.dashboardTitle ? serviceOpts.dashboardTitle : serviceName
    });

    function lineGraph(opts) {
        var graph = new grafana.Panels.Graph(opts);
        graph.state.fill = 0;

        return graph;
    }

    function createSystemRow() {
        var row = new grafana.Row();
        row.state.title = 'SYSTEM';
        row.state.showTitle = true;

        row.addPanel(lineGraph({
            title: 'Process RSS',
            span: 12,
            targets: getTargets('system.process-rss', true),
            legend: {
                show: false
            },
            lines: true,
            datasource: 'influxdb'
        }));

        return row;
    }

    function createGossipRow() {
        var row = new grafana.Row();
        row.state.title = 'GOSSIP';
        row.state.showTitle = true;

        row.addPanel(new grafana.Panels.SingleStat({
            title: 'Cluster Size',
            span: 6,
            targets: getTargets('gossip.cluster-size', false),
            legend: {
                show: false
            },
            datasource: 'influxdb'
        }));

        row.addPanel(new grafana.Panels.Graph({
            title: 'Protocol Frequency',
            span: 6,
            targets: getTargets('gossip.protocol-freq', true),
            legend: {
                show: false
            },
            datasource: 'influxdb'
        }));

        row.addPanel(lineGraph({
            title: 'Ping TX/sec',
            span: 4,
            targets: getTargets('gossip.ping-send', true),
            legend: {
                show: false
            },
            datasource: 'influxdb'
        }));

        row.addPanel(lineGraph({
            title: 'Ping RX/sec',
            span: 4,
            targets: getTargets('gossip.ping-recv', true),
            legend: {
                show: false
            },
            datasource: 'influxdb'
        }));

        row.addPanel(lineGraph({
            title: 'Ping Response Times',
            span: 4,
            targets: getTargets(['gossip.ping-timer-mean', 'gossip.ping-timer-std'], true),
            datasource: 'influxdb'
        }));

        return row;
    }

    function createBootstrapRow() {
        var row = new grafana.Row();
        row.state.title = 'BOOTSTRAP';
        row.state.showTitle = true;

        row.addPanel(new grafana.Panels.Graph({
            title: 'Join Times',
            span: 12,
            targets: getTargets('bootstrap.join', true),
            legend: {
                show: false
            },
            datasource: 'influxdb'
        }));

        return row;
    }

    function createMembershipRow() {
        var row = new grafana.Row();
        row.state.title = 'MEMBERSHIP';
        row.state.showTitle = true;

        row.addPanel(lineGraph({
            title: 'Updates',
            span: 6,
            targets: getTargets(['membership.membership-update-alive', 'membership.membership-update-suspect', 'membership.membership-update-faulty'], true),
            datasource: 'influxdb'
        }));

        row.addPanel(new grafana.Panels.Graph({
            title: 'Checksum Compute Times',
            span: 6,
            targets: getTargets(['membership.compute-checksum-count', 'membership.compute-checksum-upper'], true),
            datasource: 'influxdb'
        }));

        return row;
    }

    function createDisseminationRow() {
        var row = new grafana.Row();
        row.state.title = 'DISSEMINATION';
        row.state.showTitle = true;

        row.addPanel(new grafana.Panels.SingleStat({
            title: 'Max Piggyback',
            span: 12,
            targets: getTargets('dissemination.max-piggyback', false),
            datasource: 'influxdb'
        }));

        return row;
    }

    var functions = serviceOpts.functions;
    function createFunctionsRow() {
        var row = new grafana.Row();
        row.state.title = 'FUNCTIONS';
        row.state.showTitle = true;

        var span = functions.length % 3 === 0 ? 4 : functions.length >= 4 ? 3 : 12/functions.length;

        for (var i = 0; i < functions.length; i++) {
            var f = functions[i];
            row.addPanel(new grafana.Panels.Graph({
                title: f,
                span: span,
                targets: getTargets('functions.count', true, f),
                legend: {
                    show: false
                },
                datasource: 'influxdb'
            }));
        }

        return row;
    }

    dashboard.addRow(createSystemRow());
    dashboard.addRow(createBootstrapRow());
    dashboard.addRow(createGossipRow());
    dashboard.addRow(createMembershipRow());
    dashboard.addRow(createDisseminationRow());
    dashboard.addRow(createFunctionsRow());

    grafana.publish(dashboard);
};

function missingParameter(data, requiredPoperties) {
    for (var i = 0; i < requiredPoperties.length; i++) {
        var prop = requiredPoperties[i];
        if (!data.hasOwnProperty(prop) ||
            data[prop] === null || data[prop] === undefined) {
            return prop;
        }
    }
    return false;
}

module.exports = DashboardGen;
